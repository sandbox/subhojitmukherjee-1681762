$Id: README.txt,v 1.0 2012/10/07 12:00:00 $

This is a module for calculating California State Sales tax in
Ubercart, in Drupal. 

It is recommended to use the latest version of Ubercart 6.x with this
module. Ubercart 6.x-2.0 and later versions store MUCH more
information about tax calculations in the database, for much better
reporting. You will need this information when you complete your
quarterly excise tax return.


INSTALLATION AND SETUP

a) Copy the module on the Ubercart folder and enable it from admin panel.

b) Visit your Ubercart Store Administration page, Configuration
section, and choose "California state sales tax settings". (path:
admin/store/settings/uc_tax_ca)

c) On this page, settings: 
   - Default sales tax rate, in case address lookup fails
   - Product types to be taxed

    
OPERATION OF MODULE

Once you install and configure the module, if someone makes an order
and the Delivery Address is a California address, all taxable products
and line items that you configured will be taxed according to the
customer's tax rate. If the order has no shippable items, Ubercart
will not collect a delivery address, so the Billing Address is used.

Once the rate is found, the tax is calculated by multiplying the
product quantity * price * rate for each taxable product in the order,
plus line item price * rate for each taxable line item, and the
Ubercart Tax module handles actually adding the tax line item to the
order.

uc_tax_ca is a custom module that we have created for calculating Sales Tax of California. It consists a default csv file to calculate the Sales Tax.

Scope:
1. Drupal Module to calculate California Sales Tax by ZIP code from a CSV file with admin panel control.
2. Admin Panel Feature:
	Default tax rate 			-> Set your default tax rate when the user not for California state.
	Wholesale roles, to exclude from tax	-> Exclude a particular user type from sales tax.
	Taxed product types			-> Select Product types to apply California state sales taxes.
	Upload CSV				-> Upload a CSV file for calculate tax rate from this file. Please follow the format below:
						CSV columns: z2t_ID, ZipCode, SalesTaxRate, RateState, ReportingCodeState, RateCounty, ReportingCodeCounty, RateCity, ReportingCodeCity, RateSpecialDistrict, ReportingCodeSpecialDistrict, City, PostOffice, State, County, PrimaryRecord


3. User END:
	California Sales Tax are calculated when the user enters pincode in california range and selected State is California.
	If user is not from California and no default tax rates are defined, Sales Tax will not added
	If default tax rate is defined and User not from California, default Sales Tax will added.
	When no ZIP code found in the range of CSV files, default Sales Tax will added.
